const { validationResult } = require('express-validator');
const { User } = require('../models/models');

exports.getRegister = (req, res) => {
  return res.render('register');
}

exports.getLogin = (req, res) => {
  const errorMessage = req.flash('error');
  return res.render('login', { errorMessage });
}

exports.addUser = async (req, res) => {
  const { name, email, password } = req.body;
  await User.create({ name, email, password });
  return res.redirect('/login');
}

exports.login = async (req, res) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const errorMessage = errors.array().map(error => error.msg).join('\n');
    req.flash('error', errorMessage);
    return res.redirect('/login');
  }

  const { email, password } = req.body;
  const user = await User.findOne({ where: { email } });
  if (!user) {
    req.flash('error', 'Email invalido');
    return res.redirect('/login');
  }
  const isEqual = await user.comparePassword(password);
  if (isEqual) {
    req.session.userId = user.id;
    req.session.save(err => {
      if (err) return res.redirect('/login');
      return res.redirect('/');
    });
  }else {
    req.flash('error', 'Credenciales no coinciden');
    return res.redirect('/login');
  }
}

exports.logout = (req, res) => {
  req.session.destroy(err => {
    return res.redirect('/login')
  })
}
