const { Task } = require('../models/models');

exports.getHome = async (req, res) => {
  const tasks = await req.user.getTasks();
  return res.render('home', { showButton: true, tasks });
}

exports.getNew = (req, res) => res.render('new');

exports.getDetails = async (req, res) => {
  const { id } = req.params;
  const task = await Task.findByPk(id);
  return res.render('details', { task, showDeleteButton: task !== null });
}

exports.addTask = async (req, res) => {
  const { title, description } = req.body;
  const task = await Task.create({ title, description });
  await req.user.addTask(task);
  return res.redirect('/');
}

exports.deleteTask = async (req, res) => {
  const { id } = req.body;
  const task = await Task.findByPk(id);
  await task.destroy();
  return res.redirect('/');
}
