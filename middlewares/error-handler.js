const errorHandler = (error, req, res, next) => {
  return res.render('error', { error });
}

module.exports = errorHandler;
