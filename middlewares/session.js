const { User } = require('../models/models');

exports.isAuth = async (req, res, next) => {
  const { userId } = req.session;
  const user = await User.findByPk(userId);
  if (!user) return res.redirect('/login');
  req.user = user;
  next();
}
