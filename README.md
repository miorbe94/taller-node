# Curso de Node con express

Para poder ejecutar el proyecto hay que bajarlo y ejecutar el comando `npm install` para que regenere la carpeta `node_modules` ya que esa no se sube al repositorio.

### Tambien les adjunto algunos link a fuentes importantes
- [NPM](https://www.npmjs.com/) *Repositorio de paquetes de Node*
- [Express](https://expressjs.com/) *Framework de Node*
- [Pug](https://pugjs.org/api/getting-started.html) *Sistema de plantillas*
- [Sequelize](https://sequelize.org/) *ORM*
- [bcrypt](https://www.npmjs.com/package/bcrypt) *Paquete para encriptar*
- [express-session](https://www.npmjs.com/package/express-session) *Variables de sesión*
- [connect-flash](https://www.npmjs.com/package/connect-flash) *Mensajes flash*
- [express-validator](https://express-validator.github.io/docs/) *Validaciones*


>#### Importante
>
>Para mi configuración de la base de datos `db.js` yo estoy usando el dialecto `postgres`. Si tu usas mysql cambialo por `mysql`.
>
>>Tambien hay que cambiar los paquetes que se usan para la conexion.
>>
>>**Para desinstalar los de postgres**
>>`npm uninstall pg pg-hstore`
>>
>>**Para instalar los de mysql**
>>`npm install mysql2`


Si me faltó agregar algo por favor haganmelo saber.
