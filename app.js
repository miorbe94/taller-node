// Se importan dependencias
const express = require('express');
const session = require('express-session');
const flash = require('connect-flash');

// Se importan dependencias creadas por nosotros
const taskRouter = require('./routes/tasks');
const userRouter = require('./routes/user');
const errorHandler = require('./middlewares/error-handler');

// Creamos instancia de express
const app = express();

app.use(express.static('public'));

// Poder recibir valores en el body
app.use(express.urlencoded({ extended: false }));

app.use(session({
  secret: 'esteesmisecreto',
  resave: false,
  saveUninitialized: false
}));
app.use(flash());

// Se agregan rutas
app.use(taskRouter);
app.use(userRouter);

app.use((req, res) => res.render('notfound'));
app.use(errorHandler);


// Setea el sistema de plantillas
app.set('view engine', 'pug');

// Se exporta instancia app
module.exports = app;
