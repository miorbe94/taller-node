// Se requiere la instancia de app y se nuestra db
const app = require('./app');
const sequelize = require('./db');

(async () => {
  await sequelize.sync();
  console.log('Connection has been established successfully.');
  await app.listen(3000);
  console.log('Corriendo en el puerto', 3000);
})();
