const Sequelize = require('sequelize');
const sequelize = require('../db');
const bcrypt = require('bcrypt');

class User extends Sequelize.Model {
  async comparePassword(password) {
    return bcrypt.compare(password, this.password);
  }
}
User.init({
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
  }
}, {
  hooks: {
    beforeSave: async (user, options) => {
      user.password = await bcrypt.hash(user.password, 10);
    },
  },
  sequelize,
  modelName: 'user'
})

class Task extends Sequelize.Model {}
Task.init({
  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  description: Sequelize.TEXT,
}, { sequelize, modelName: 'task' })

User.hasMany(Task);
Task.belongsTo(User);

module.exports = {
  User,
  Task,
}
