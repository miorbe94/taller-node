// Se requiere el router desde express
const { Router } = require('express');

// Se requiere el controlador
const taskController = require('../controllers/task.controller');
const { isAuth } = require('../middlewares/session');

// Se crea instancia del router
const router = Router();

// Se agregan las rutas
router.get('/', isAuth, taskController.getHome);
router.get('/new', taskController.getNew);
router.post('/tasks', isAuth, taskController.addTask);
router.get('/tasks/:id', taskController.getDetails);
router.post('/tasks/delete', taskController.deleteTask);

// Se exporta el router
module.exports = router;
