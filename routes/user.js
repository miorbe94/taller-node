const { Router } = require('express');
const { body } = require('express-validator');

const userController = require('../controllers/user.controller');

const router = Router();

router.get('/register', userController.getRegister);
router.get('/login', userController.getLogin);
router.post('/register', userController.addUser);
router.post('/login', [
  body('email').isEmail().withMessage('El correo no es válido.'),
  body('password').isLength({ min: 5 }).withMessage('El password debe contener al menos 5 caracteres'),
],userController.login);
router.post('/logout', userController.logout);

module.exports = router;
